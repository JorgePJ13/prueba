package es.unex.gps_proyect.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.squareup.picasso.Picasso;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.R;
import es.unex.gps_proyect.InjectorUtils;

public class EditarReceta_Activity extends AppCompatActivity {

    private static final int PICK_PHOTO_FOR_AVATAR = 1;
    private EditText EDescripcion, EMinutes, EPuntuacion, ETitulo, EImageRec, EComensales;
    private ImageView EImage, EImageReceta;
    private CheckBox EVege, EVegan, EGlutenF, ELacteo, ESaludable;
    private Receta recetaAux;
    private Button EAnadir;
    private EditarViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_receta);

        //Localizacion de campos
        EDescripcion = (EditText) findViewById(R.id.EDescripcionReceta);
        ETitulo = (EditText) findViewById(R.id.TituloReceta);
        EImage = (ImageView) findViewById(R.id.Atras);
        EImageReceta = (ImageView) findViewById(R.id.imageReceta);
        EPuntuacion = (EditText) findViewById(R.id.puntuacionUsuario);
        EAnadir = (Button) findViewById(R.id.AnadirReceta);
        EMinutes = (EditText) findViewById(R.id.Preparacion);
        EImageRec = (EditText) findViewById(R.id.urlImagen);
        EComensales = (EditText) findViewById(R.id.Raciones);
        EVege = (CheckBox) findViewById(R.id.vegetariana);
        EVegan = (CheckBox) findViewById(R.id.Vegana);
        EGlutenF = (CheckBox) findViewById(R.id.sinGluten);
        ELacteo = (CheckBox) findViewById(R.id.sinLacteos);
        ESaludable = (CheckBox) findViewById(R.id.Saludable);

        recetaAux = (Receta) getIntent().getSerializableExtra("RecetaEdit");

        //Imagen de la receta mediante librería Picasso
        Picasso.with(this).load(recetaAux.getImage()).into(EImageReceta);

        //Control de acciones Text, interfaz con contenido
        ETitulo.setText(recetaAux.getTitle());

        //Para Scroll
        EDescripcion.setMovementMethod(new ScrollingMovementMethod());
        if(recetaAux.getDescripcion()==null){
            EDescripcion.setText("Descripción de la receta");
        }else{
            EDescripcion.setText(recetaAux.getDescripcion());
        }

        if(recetaAux.getPuntuacion()==null){
            EPuntuacion.setText("Puntuación del usuario sobre la receta");
        }else{
            EPuntuacion.setText(recetaAux.getPuntuacion().toString());
        }

        if(recetaAux.getReadyInMinutes()==null){
            EMinutes.setText("Tiempo de preparación");
        }else{
            EMinutes.setText(recetaAux.getReadyInMinutes().toString());
        }

        EImageRec.setText(recetaAux.getImage());

        if(recetaAux.getServings()==null){
            EComensales.setText("Número de comensales");
        }else{
            EComensales.setText(recetaAux.getServings().toString());
        }

        //Control de Checkbox
        if(recetaAux.getVegetarian()==true){
            EVege.setChecked(true);
        }

        if(recetaAux.getVegan()==true){
            EVegan.setChecked(true);
        }

        if(recetaAux.getGlutenFree()==true){
            EGlutenF.setChecked(true);
        }

        if(recetaAux.getDairyFree()==true){
            ELacteo.setChecked(true);
        }

        if(recetaAux.getVeryHealthy()==true){
            ESaludable.setChecked(true);
        }

        EditarViewModelFactory factory = InjectorUtils.provideEditarViewModelFactory(this.getApplicationContext());
        mViewModel = ViewModelProviders.of(this, factory).get(EditarViewModel.class);

        EAnadir.setOnClickListener(new View.OnClickListener() { // hago clic en el botón
            @Override
            public void onClick(View v) {
                if(Integer.parseInt(EPuntuacion.getText().toString()) <= 10) {
                    recetaAux.setTitle(ETitulo.getText().toString());
                    recetaAux.setPuntuacion(Integer.parseInt(EPuntuacion.getText().toString()));
                    recetaAux.setDescripcion(EDescripcion.getText().toString());
                    recetaAux.setImage(EImageRec.getText().toString());
                    recetaAux.setServings(Integer.parseInt(EComensales.getText().toString()));
                    recetaAux.setReadyInMinutes(Integer.parseInt(EMinutes.getText().toString()));
                    recetaAux.setVegan(EVegan.isChecked());
                    recetaAux.setVegetarian(EVege.isChecked());
                    recetaAux.setGlutenFree(EGlutenF.isChecked());
                    recetaAux.setDairyFree(ELacteo.isChecked());
                    recetaAux.setVeryHealthy(ESaludable.isChecked());
                    mViewModel.updateRec(recetaAux);
                    Toast.makeText(getApplicationContext(), "Receta Actualizada.", Toast.LENGTH_LONG).show();
                    //Volvemos a fragment home
                    Intent intent = new Intent(getApplicationContext(), InicioActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(), "La puntuación ha de estar entre 0 y 10", Toast.LENGTH_LONG).show();
                }
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarID);

        //Implementacion boton hacia atras
        EImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

}