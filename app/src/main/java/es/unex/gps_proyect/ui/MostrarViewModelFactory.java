package es.unex.gps_proyect.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.gps_proyect.data.Repository;

public class MostrarViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final Repository mRepository;
    private final Integer mId;

    public MostrarViewModelFactory(Repository repository, Integer id) {
        this.mRepository = repository;
        this.mId = id;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new MostrarViewModel(mRepository, mId);
    }
}