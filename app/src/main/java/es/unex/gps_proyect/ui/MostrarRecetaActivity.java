package es.unex.gps_proyect.ui;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.R;
import es.unex.gps_proyect.InjectorUtils;

public class MostrarRecetaActivity extends AppCompatActivity {

    private static final int PICK_PHOTO_FOR_AVATAR = 1;
    private TextView EDescripcion, EMinutes, EPuntuacion, EComensales, ETitulo, EImageRec, EWeb, ELikes, EPuntosWeb;
    private ImageView EImage, EImageRece;
    private CheckBox EVege, EVegan, EGlutenF, ELacteo, ESaludable;
    FloatingActionButton EFbutton;
    private Receta recetaAux;
    private MostrarViewModel mViewModel;
    private Integer id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_receta);

        EDescripcion = (TextView) findViewById(R.id.DescripReceMostrar);
        ETitulo = (TextView) findViewById(R.id.RecetaTitulo);
        EImage = (ImageView) findViewById(R.id.Atras);
        EImageRece = (ImageView) findViewById(R.id.imageView3);
        EPuntuacion = (TextView) findViewById(R.id.PuntuacionUsuarioReceta);
        EMinutes = (TextView) findViewById(R.id.TiempoPreparacionRece);
        EComensales = (TextView) findViewById(R.id.NumeroRacionesRece);
        EVege = (CheckBox) findViewById(R.id.IsVege);
        EVegan = (CheckBox) findViewById(R.id.IsVega);
        EGlutenF = (CheckBox) findViewById(R.id.sinGlutenRece);
        ELacteo = (CheckBox) findViewById(R.id.sinLacteosRece);
        ESaludable = (CheckBox) findViewById(R.id.IsSalud);
        EWeb = (TextView) findViewById(R.id.SitioWEB);
        ELikes = (TextView) findViewById(R.id.LikesWEB);
        EPuntosWeb = (TextView) findViewById(R.id.PuntuacionSpoonacular);

        EFbutton = (FloatingActionButton) findViewById(R.id.floatingButton);



        //Para Scroll
        EDescripcion.setMovementMethod(new ScrollingMovementMethod());
        recetaAux = (Receta) getIntent().getSerializableExtra("Receta");
        id = recetaAux.getId();

        //Imagen de la receta mediante librería Picasso
        Picasso.with(this).load(recetaAux.getImage()).into(EImageRece);

        //Control de acciones Text, interfaz con contenido
        ETitulo.setText(recetaAux.getTitle());

        if(recetaAux.getDescripcion()==null){
            EDescripcion.setText(Html.fromHtml(recetaAux.getSummary(), Html.FROM_HTML_MODE_COMPACT));
        }else{
            EDescripcion.setText(recetaAux.getDescripcion());
        }
        //Para Scroll
        EDescripcion.setMovementMethod(new ScrollingMovementMethod());

        if(recetaAux.getPuntuacion()==null){
            EPuntuacion.setText("N/D");
        }else{
            EPuntuacion.setText(recetaAux.getPuntuacion().toString());
        }

        if(recetaAux.getReadyInMinutes()==null){
            EMinutes.setText("Tiempo de preparación");
        }else{
            EMinutes.setText(recetaAux.getReadyInMinutes().toString()  + " min");
        }

        if(recetaAux.getServings()==null){
            EComensales.setText("Número de comensales");
        }else{
            EComensales.setText(recetaAux.getServings().toString());
        }


        EVege.setEnabled(false);
        EVegan.setEnabled(false);
        EGlutenF.setEnabled(false);
        ELacteo.setEnabled(false);
        ESaludable.setEnabled(false);

        //Control de Checkbox
        if(recetaAux.getVegetarian()==true){
            EVege.setChecked(true);

        }

        if(recetaAux.getVegan()==true){
            EVegan.setChecked(true);
        }

        if(recetaAux.getGlutenFree()==true){
            EGlutenF.setChecked(true);
        }

        if(recetaAux.getDairyFree()==true){
            ELacteo.setChecked(true);
        }

        if(recetaAux.getVeryHealthy()==true){
            ESaludable.setChecked(true);
        }

        if(recetaAux.getSpoonacularScore()==null){
            EPuntosWeb.setText("N/D");
        }else{
            EPuntosWeb.setText(recetaAux.getSpoonacularScore().toString());
        }

        if(recetaAux.getAggregateLikes()==null){
            ELikes.setText("N/D");
        }else{
            ELikes.setText(recetaAux.getAggregateLikes().toString());
        }

        if(recetaAux.getSpoonacularSourceUrl()!=null) {
            EWeb.setText(recetaAux.getSpoonacularSourceUrl());
        }

        if(recetaAux.isEsFav() == true){
            EFbutton.setImageResource(R.drawable.ic_baseline_favorite_25);
        }

        //Instancia de Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarID);
        toolbar.setSubtitle("DIMAKER");
        toolbar.inflateMenu(R.menu.menu);

        MostrarViewModelFactory factory = InjectorUtils.provideMostrarViewModelFactory(this.getApplicationContext(), id);
        mViewModel = ViewModelProviders.of(this, factory).get(MostrarViewModel.class);
        mViewModel.getReceta().observe(this, mReceta -> {
            if (mReceta != null) {
                recetaAux = mReceta;
            }
        });

        //Implementacion boton hacia atras
        EImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //Menu
        opcionesMenu(toolbar);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //Volvemos a fragment
        onBackPressed();
        return false;
    }

    //Eliminamos la receta seleccionada
    public void EliminarR(MenuItem item) {
        mViewModel.deleteRec();
        Toast.makeText(getApplicationContext(), "Receta Eliminada", Toast.LENGTH_LONG).show();
        //Volvemos a fragment
        onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setVisible(true);
        }
        return true;
    }

    //Anade una receta a favororita, asi como la elimina
    public void Favorita(View view) {
        if (recetaAux.isEsFav() == false) {
            recetaAux.setEsFav(true); //Favorita
            mViewModel.setEsFav(); //Añadida para mostrar en favoritas
            EFbutton.setImageResource(R.drawable.ic_baseline_favorite_25);
            Toast.makeText(getApplicationContext(), "Receta Añadida a Favoritas", Toast.LENGTH_LONG).show();
        } else {
            recetaAux.setEsFav(false); //Favorita
            mViewModel.setNoEsFav();
            EFbutton.setImageResource(R.drawable.ic_baseline_favorite_24);
            Toast.makeText(getApplicationContext(), "Receta Eliminada de Favoritas", Toast.LENGTH_LONG).show();
        }
    }

    public void opcionesMenu(Toolbar toolbar) {
        //Con esto se evita que el usuario tenga acceso a otras recetas
        if (recetaAux.isEsUsua() == false) {
            toolbar.getMenu().findItem(R.id.Editar).setVisible(false);
            toolbar.getMenu().findItem(R.id.Eliminar).setVisible(false);
        }
        //Menu de opciones
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.Eliminar) {
                    AlertDialog.Builder alerta = new AlertDialog.Builder(MostrarRecetaActivity.this);
                    alerta.setMessage("¿Desea eliminar la receta?")
                            .setCancelable(false)
                            .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    EliminarR(item); //Eliminamos la receta de la base de datos
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                    AlertDialog titulo = alerta.create();
                    titulo.setTitle("Eliminar Receta");
                    titulo.show();
                } else if (item.getItemId() == R.id.Editar) {
                    Intent intent = new Intent(getApplicationContext(), EditarReceta_Activity.class);
                    intent.putExtra("RecetaEdit", recetaAux);
                    startActivity(intent);
                } else if (item.getItemId() == R.id.Puntuar) {
                    EditText Puntuacion;
                    Puntuacion = new EditText(MostrarRecetaActivity.this);
                    Puntuacion.setInputType(InputType.TYPE_CLASS_NUMBER);
                    AlertDialog.Builder alertaP = new AlertDialog.Builder(MostrarRecetaActivity.this);
                    alertaP.setView(Puntuacion)
                            .setMessage("Asignar puntuación a la receta.")
                            .setCancelable(false)
                            .setPositiveButton("Puntuar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (recetaAux.isEsUsua() == true) {
                                        if (Integer.parseInt(Puntuacion.getText().toString()) <= 10) {
                                            recetaAux.setPuntuacion(Integer.parseInt(Puntuacion.getText().toString()));
                                            mViewModel.setPuntuacion(Integer.parseInt(Puntuacion.getText().toString()));
                                            //Volvemos a fragment home
                                            onBackPressed();
                                            Toast.makeText(getApplicationContext(), "Receta Actualizada.", Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "La puntuación ha de estar entre 0 y 10", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        if (Integer.parseInt(Puntuacion.getText().toString()) <= 10) {
                                            recetaAux.setEsUsua(false);
                                            recetaAux.setPuntuacion(Integer.parseInt(Puntuacion.getText().toString()));
                                            mViewModel.setNoEsUsua();
                                            mViewModel.setPuntuacion(Integer.parseInt(Puntuacion.getText().toString()));
                                            //Volvemos a fragment home
                                            onBackPressed();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "La puntuación ha de estar entre 0 y 10", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            })
                            .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                    AlertDialog tituloP = alertaP.create();
                    tituloP.setTitle("Puntuar Receta");
                    tituloP.show();
                }
                return false;
            }
        });
    }
}