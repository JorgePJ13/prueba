package es.unex.gps_proyect.ui;

import androidx.lifecycle.ViewModel;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.data.Repository;

public class CrearViewModel extends ViewModel {

    private final Repository mRepository;

    public CrearViewModel(Repository repository) {
        mRepository = repository;
    }

    public void insertRec(Receta receta) {
        mRepository.addReceta(receta);
    }

}
