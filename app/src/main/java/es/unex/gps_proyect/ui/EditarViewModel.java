package es.unex.gps_proyect.ui;

import androidx.lifecycle.ViewModel;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.data.Repository;

public class EditarViewModel extends ViewModel {

    private final Repository mRepository;

    public EditarViewModel(Repository repository) {
        mRepository = repository;
    }

    public void updateRec(Receta receta) {
        mRepository.updateReceta(receta);
    }

}
