package es.unex.gps_proyect.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.data.Repository;

public class HomeViewModel extends ViewModel {

    private final Repository mRepository;
    private LiveData<List<Receta>> mRecetas;
    private LiveData<List<Receta>> mRecetasTop;

    public HomeViewModel(Repository repository) {
        mRepository = repository;
        mRepository.getAll();//API
        mRecetas = repository.getRecetas();
        mRecetasTop = repository.getTop();
    }

    public LiveData<List<Receta>> getRecetas() {
        return mRecetas;
    }

    public LiveData<List<Receta>> getTop() {
        return mRecetasTop;
    }
}
