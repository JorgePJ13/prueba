package es.unex.gps_proyect.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.gps_proyect.data.Repository;

public class CrearViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final Repository mRepository;

    public CrearViewModelFactory(Repository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new CrearViewModel(mRepository);
    }
}