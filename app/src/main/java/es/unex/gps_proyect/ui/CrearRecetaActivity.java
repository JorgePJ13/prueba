package es.unex.gps_proyect.ui;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.R;
import es.unex.gps_proyect.InjectorUtils;

public class CrearRecetaActivity extends AppCompatActivity {

    private static final int PICK_PHOTO_FOR_AVATAR = 1;
    private EditText EDescripcion, EMinutes, EPuntuacion, ETitulo, EImageRec, EComensales;
    private ImageView EImage, EImageReceta;
    private CheckBox EVege, EVegan, EGlutenF, ELacteo, ESaludable;
    private Receta recetaAux;
    private Button EAnadir;
    private CrearViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_receta);

        //Localizacion de campos
        EDescripcion = (EditText) findViewById(R.id.EDescripcionReceta);
        ETitulo = (EditText) findViewById(R.id.TituloReceta);
        EImage = (ImageView) findViewById(R.id.Atras);
        EImageReceta = (ImageView) findViewById(R.id.imageReceta);
        EPuntuacion = (EditText) findViewById(R.id.puntuacionUsuario);
        EAnadir = (Button) findViewById(R.id.AnadirReceta);
        EMinutes = (EditText) findViewById(R.id.Preparacion);
        EImageRec = (EditText) findViewById(R.id.urlImagen);
        EComensales = (EditText) findViewById(R.id.Raciones);
        EVege = (CheckBox) findViewById(R.id.vegetariana);
        EVegan = (CheckBox) findViewById(R.id.Vegana);
        EGlutenF = (CheckBox) findViewById(R.id.sinGluten);
        ELacteo = (CheckBox) findViewById(R.id.sinLacteos);
        ESaludable = (CheckBox) findViewById(R.id.Saludable);

        //Para Scroll
        EDescripcion.setMovementMethod(new ScrollingMovementMethod());

        //Imagen por defecto al crear receta
        EImageReceta.setImageResource(R.mipmap.comida);

        CrearViewModelFactory factory = InjectorUtils.provideCrearViewModelFactory(this.getApplicationContext());
        mViewModel = ViewModelProviders.of(this, factory).get(CrearViewModel.class);

        EAnadir.setOnClickListener(new View.OnClickListener() { // hago clic en el botón
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(EPuntuacion.getText().toString()) <= 10) {
                    String image;
                    if(EImageRec.getText().toString().isEmpty()){
                        image = "https://blogs.unitec.mx/content/dam/blogs/imagenes/corp_samara/el-plato-del-bien-comer-1.jpg";
                    }else{
                        image = EImageRec.getText().toString();
                    }
                    recetaAux = new Receta(ETitulo.getText().toString(), image, Integer.parseInt(EComensales.getText().toString()), Integer.parseInt(EMinutes.getText().toString()), EVege.isChecked(), EVegan.isChecked(), EGlutenF.isChecked(), ELacteo.isChecked(), ESaludable.isChecked(), EDescripcion.getText().toString(), Integer.parseInt(EPuntuacion.getText().toString()));
                    mViewModel.insertRec(recetaAux);
                    Toast.makeText(getApplicationContext(), "Receta Creada", Toast.LENGTH_LONG).show();
                    onBackPressed();//Volvemos a receta de usuario
                } else {
                    Toast.makeText(getApplicationContext(), "La puntuación ha de estar entre 0 y 10", Toast.LENGTH_LONG).show();
                }
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarID);

        //Implementacion boton hacia atras
        EImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}