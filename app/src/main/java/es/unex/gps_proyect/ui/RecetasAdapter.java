package es.unex.gps_proyect.ui;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.unex.gps_proyect.R;
import es.unex.gps_proyect.data.Model.Receta;


public class RecetasAdapter extends RecyclerView.Adapter<RecetasAdapter.ViewHolder> {
    private List<Receta> mRecetas;
    private ItemClickListener onItemClickListener;
    private Activity activity;

    public interface ItemClickListener {
        void onItemClick(Receta receta);
    }

    public void setItemClickListener(ItemClickListener clickListener) {
        onItemClickListener = clickListener;
    }

    public RecetasAdapter(List<Receta> myDataset, Activity activi) {
        mRecetas=myDataset;
        activity=activi;
    }

    public List<Receta> getRecetas() {
        return mRecetas;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // - Inflate the View for every element
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.receta_item, parent, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mRecetas.get(position), onItemClickListener);
        holder.title.setText(mRecetas.get(position).getTitle());
        holder.EComensales.setText("Raciones: " + mRecetas.get(position).getServings().toString() + " personas");
        holder.EMinutes.setText("Tiempo de preparación: " + mRecetas.get(position).getReadyInMinutes().toString() + " minutos");
        Picasso.with(activity).load(mRecetas.get(position).getImage()).into(holder.EImage);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mRecetas.size();
    }

    public void addAll(List<Receta> items) {
        mRecetas = new ArrayList<Receta>();
        mRecetas.addAll(0, items);
        notifyDataSetChanged();

    }

    public void clear(){
        mRecetas.clear();
        notifyDataSetChanged();

    }

    public void load(List<Receta> items){
        mRecetas = new ArrayList<Receta>();
        mRecetas.clear();
        mRecetas = items;
        notifyDataSetChanged();

    }

    public Receta getItem(int pos) { return mRecetas.get(pos); }

    public void swap(List<Receta> datos){
        mRecetas = datos;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View mView;
        private TextView title, EComensales, EMinutes;
        private ImageView EImage;

        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;

            title = (TextView) itemView.findViewById(R.id.NombreReceta);
            EComensales = (TextView) itemView.findViewById(R.id.comensalesReceta);
            EMinutes = (TextView) itemView.findViewById(R.id.MinutesRece);
            EImage = (ImageView) itemView.findViewById(R.id.Foto_Receta);
        }

        public void bind(final Receta receta, final ItemClickListener onItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    onItemClickListener.onItemClick(receta);
                }
            });

        }
    }

    public void filterList(List<Receta>filtradas){
        mRecetas.clear();
        mRecetas.addAll(filtradas);
        notifyDataSetChanged();
    }

    @Override
    public String toString() {
        return "ListaRecetas{" +
                "data=" + mRecetas +
                '}';
    }

}
