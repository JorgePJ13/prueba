package es.unex.gps_proyect.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.R;
import es.unex.gps_proyect.InjectorUtils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_Home#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_Home extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ImageView EImage;
    private RecyclerView rRecetas;
    private FloatingActionButton fab;
    private RecetasAdapter Adapter;
    private EditText Buscador;
    private List<Receta> Recetas = new ArrayList<>();

    private HomeViewModel mViewModel;

    public Fragment_Home() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment_Home.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment_Home newInstance(String param1, String param2) {
        Fragment_Home fragment = new Fragment_Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment__home, container, false);

        EImage = (ImageView) vista.findViewById(R.id.AyudaHome);
        rRecetas = (RecyclerView) vista.findViewById(R.id.recetasRecycler);
        rRecetas.setLayoutManager(new LinearLayoutManager(getContext()));
        Toolbar toolbar = (Toolbar) vista.findViewById(R.id.toolbarID);

        //Mostrar TOP Recetas
        fab = vista.findViewById(R.id.floatingButton3);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarTopReceta();
            }
        });

        //Ayuda de Usuario
        EImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
                alerta.setMessage("\n1º- En la pestaña actual podrá interactuar con el total de las recetas que conforman esta app, así como observar" +
                        " las recetas que ha puntuado y las cuales se mostrarán en el TOP, recuerde, en el TOP se muestran el conjunto de " +
                        "recetas con una puntuación mayor o igual que 5, todas ellas oredenadas de forma descendente. \n \n" +
                        "2º- Para acceder al TOP (ranking de recetas) nombrado anteriormente, sólo ha de pulsar el botón localizado abajo a su derecha," +
                        " el cual indica a su vez el número de las recetas que conforman ese ranking.")
                        .setCancelable(false)
                        .setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                AlertDialog titulo = alerta.create();
                titulo.setTitle("Mensaje de Ayuda");
                titulo.show();
            }
        });

        Buscador = vista.findViewById(R.id.Barra_Busc);
        Buscador.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty() == false) {
                    filtrar(editable.toString());
                } else {
                    Adapter.clear();
                    Adapter.addAll(Recetas);
                }
            }
        });

        //Inicializar el Adapter
        List<Receta> conjuntoRecetas = new ArrayList<Receta>();
        Adapter = new RecetasAdapter(conjuntoRecetas, getActivity());
        Adapter.setItemClickListener(new RecetasAdapter.ItemClickListener() {
            public void onItemClick(Receta receta) {
                Intent intent = new Intent(getContext(), MostrarRecetaActivity.class);
                intent.putExtra("Receta", receta);
                startActivity(intent);
            }

        });
        rRecetas.setAdapter(Adapter);

        HomeViewModelFactory factory = InjectorUtils.provideHomeViewModelFactory(getActivity().getApplicationContext());
        mViewModel = ViewModelProviders.of(getActivity(), factory).get(HomeViewModel.class);
        mViewModel.getRecetas().observe(getViewLifecycleOwner(), recetas -> {
            Adapter.swap(recetas);
            //Para mantenerlas tos en el filtrado
            Recetas.clear();
            Recetas.addAll(recetas);
        });

        //Metodo utilizado para el cambio de la imagen del top favoritas
        contarFavs();

        return vista;
    }

    public void filtrar(String Texto) {
        List<Receta> filtrarLista = new ArrayList<>();
        for (Receta r : Adapter.getRecetas()) {
            if (r.getTitle().toLowerCase().contains(Texto.toLowerCase())) {
                filtrarLista.add(r);
            }
        }
        Adapter.filterList(filtrarLista);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_buscar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //Mostramos el top recetas alojadas en la aplicacion
    public void mostrarTopReceta() {
        mViewModel.getTop().observe(getViewLifecycleOwner(), recetas -> {
            Adapter.swap(recetas);
            if (recetas != null && recetas.size() > 0) {
                Toast.makeText(getContext(), "Mostrando el TOP Recetas", Toast.LENGTH_LONG).show();
                Adapter.setItemClickListener(new RecetasAdapter.ItemClickListener() {
                    public void onItemClick(Receta receta) {
                        Intent intent = new Intent(getContext(), MostrarRecetaActivity.class);
                        intent.putExtra("Receta", receta);
                        startActivity(intent);
                    }
                });
            } else {
                Toast.makeText(getContext(), "No hay Recetas en el TOP", Toast.LENGTH_LONG).show();
                Adapter.swap(Recetas);
            }
        });
    }

    //Cambio de imagen en boton de Top Favoritas
    public void contarFavs() {
        mViewModel.getTop().observe(getViewLifecycleOwner(), recetas -> {
            switch (recetas.size()) {
                case 0:
                    fab.setImageResource(R.drawable.ic_baseline_filter_none_24);
                    break;
                case 1:
                    fab.setImageResource(R.drawable.ic_baseline_filter_1_24);
                    break;
                case 2:
                    fab.setImageResource(R.drawable.ic_baseline_filter_2_24);
                    break;
                case 3:
                    fab.setImageResource(R.drawable.ic_baseline_filter_3_24);
                    break;
                case 4:
                    fab.setImageResource(R.drawable.ic_baseline_filter_4_24);
                    break;
                case 5:
                    fab.setImageResource(R.drawable.ic_baseline_filter_5_24);
                    break;
                case 6:
                    fab.setImageResource(R.drawable.ic_baseline_filter_6_24);
                    break;
                case 7:
                    fab.setImageResource(R.drawable.ic_baseline_filter_7_24);
                    break;
                case 8:
                    fab.setImageResource(R.drawable.ic_baseline_filter_8_24);
                    break;
                case 9:
                    fab.setImageResource(R.drawable.ic_baseline_filter_9_24);
                    break;
                default:
                    fab.setImageResource(R.drawable.ic_baseline_filter_9_plus_24);
            }
        });
    }
}