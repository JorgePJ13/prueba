package es.unex.gps_proyect.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.data.Repository;

public class FavoritasViewModel extends ViewModel {

    private final Repository mRepository;
    private LiveData<List<Receta>> mRecetas;
    private LiveData<List<Receta>> mRecetasTop;

    public FavoritasViewModel(Repository repository) {
        mRepository = repository;
        mRecetas = repository.getRecetasFav();
        mRecetasTop = repository.getRecetasFavTop();
    }

    public LiveData<List<Receta>> getRecetas() {
        return mRecetas;
    }

    public LiveData<List<Receta>> getTopFavs() {
        return mRecetasTop;
    }

}
