package es.unex.gps_proyect.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.R;
import es.unex.gps_proyect.InjectorUtils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_favoritas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_favoritas extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ImageView EImage;
    private RecyclerView rRecetas;
    private FloatingActionButton fab;
    private RecetasAdapter Adapter;
    private EditText Buscador;
    private List<Receta> Recetas = new ArrayList<>();
    private TextView emptyView;

    private FavoritasViewModel mViewModel;

    public Fragment_favoritas() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment_favoritas.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment_favoritas newInstance(String param1, String param2) {
        Fragment_favoritas fragment = new Fragment_favoritas();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View vista = inflater.inflate(R.layout.fragment_favoritas, container, false);

        EImage = (ImageView) vista.findViewById(R.id.Help);
        rRecetas = (RecyclerView) vista.findViewById(R.id.recetasRecyclerFav);
        emptyView = (TextView) vista.findViewById(R.id.empty_view);
        rRecetas.setLayoutManager(new LinearLayoutManager(getContext()));

        //Mostrar TOP Recetas
        fab = vista.findViewById(R.id.floatingButton2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarTopReceta();
            }
        });

        //Ayuda de Usuario
        EImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
                alerta.setMessage("\n1º- En la pestaña actual podrá interactuar con las recetas que ha marcado como favoritas, así como observar" +
                        " las recetas favoritas que ha puntuado y las cuales se mostrarán en el TOP, recuerde, en el TOP se muestran el conjunto de " +
                        "recetas con una puntuación mayor o igual que 5, todas ellas oredenadas de forma descendente. \n \n" +
                        "2º- Para acceder al TOP (ranking de recetas) nombrado anteriormente, sólo ha de pulsar el botón localizado abajo a su derecha," +
                        " el cual indica a su vez el número de las recetas que conforman ese ranking.")
                        .setCancelable(false)
                        .setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                AlertDialog titulo = alerta.create();
                titulo.setTitle("Mensaje de Ayuda");
                titulo.show();
            }
        });

        Buscador = vista.findViewById(R.id.Barra_BuscFav);
        Buscador.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty() == false) {
                    filtrar(editable.toString());
                } else {
                    Adapter.clear();
                    Adapter.addAll(Recetas);
                }
            }
        });

        List<Receta> conjuntoRecetas = new ArrayList<Receta>();
        Adapter = new RecetasAdapter(conjuntoRecetas, getActivity());
        Adapter.setItemClickListener(new RecetasAdapter.ItemClickListener() {
            public void onItemClick(Receta receta) {
                Intent intent = new Intent(getContext(), MostrarRecetaActivity.class);
                intent.putExtra("Receta", receta);
                startActivity(intent);
            }

        });


        rRecetas.setAdapter(Adapter);
        FavoritasViewModelFactory factory = InjectorUtils.provideFavsViewModelFactory(getActivity().getApplicationContext());
        mViewModel = ViewModelProviders.of(getActivity(), factory).get(FavoritasViewModel.class);
        mViewModel.getRecetas().observe(getViewLifecycleOwner(), recetas -> {
            Adapter.swap(recetas);
            //Comprobamos si no hay recetas a mostrar, indicandoselo asi al usuario
            if (recetas.isEmpty()) {
                emptyView.setText("No Hay Recetas Favoritas");
                rRecetas.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            } else {
                rRecetas.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
                Recetas.clear();
                Recetas.addAll(recetas);
            }
        });

        //Metodo utilizado para el cambio de la imagen del top favoritas
        contarFavs();

        return vista;
    }

    //Mostramos el top recetas marcadas como favoritas
    public void mostrarTopReceta() {
        mViewModel.getTopFavs().observe(getViewLifecycleOwner(), recetas -> {
            Adapter.swap(recetas);
            if (recetas != null && recetas.size() > 0) {
                Toast.makeText(getContext(), "Mostrando el TOP Recetas Favoritas", Toast.LENGTH_LONG).show();
                Adapter.setItemClickListener(new RecetasAdapter.ItemClickListener() {
                    public void onItemClick(Receta receta) {
                        Intent intent = new Intent(getContext(), MostrarRecetaActivity.class);
                        intent.putExtra("Receta", receta);
                        startActivity(intent);
                    }
                });
            } else {
                Toast.makeText(getContext(), "No hay Recetas en el TOP", Toast.LENGTH_LONG).show();
                Adapter.swap(Recetas);
            }
        });
    }

    //Filtrado de recetas de usuario
    public void filtrar(String Texto) {
        List<Receta> filtrarLista = new ArrayList<>();
        for (Receta r : Adapter.getRecetas()) {
            if (r.getTitle().toLowerCase().contains(Texto.toLowerCase())) {
                filtrarLista.add(r);
            }
        }
        Adapter.filterList(filtrarLista);
    }

    //Cambio de imagen en boton de Top Favoritas
    public void contarFavs() {
        mViewModel.getTopFavs().observe(getViewLifecycleOwner(), recetas -> {
            switch (recetas.size()) {
                case 0:
                    fab.setImageResource(R.drawable.ic_baseline_filter_none_24);
                    break;
                case 1:
                    fab.setImageResource(R.drawable.ic_baseline_filter_1_24);
                    break;
                case 2:
                    fab.setImageResource(R.drawable.ic_baseline_filter_2_24);
                    break;
                case 3:
                    fab.setImageResource(R.drawable.ic_baseline_filter_3_24);
                    break;
                case 4:
                    fab.setImageResource(R.drawable.ic_baseline_filter_4_24);
                    break;
                case 5:
                    fab.setImageResource(R.drawable.ic_baseline_filter_5_24);
                    break;
                case 6:
                    fab.setImageResource(R.drawable.ic_baseline_filter_6_24);
                    break;
                case 7:
                    fab.setImageResource(R.drawable.ic_baseline_filter_7_24);
                    break;
                case 8:
                    fab.setImageResource(R.drawable.ic_baseline_filter_8_24);
                    break;
                case 9:
                    fab.setImageResource(R.drawable.ic_baseline_filter_9_24);
                    break;
                default:
                    fab.setImageResource(R.drawable.ic_baseline_filter_9_plus_24);
            }
        });
    }

}