package es.unex.gps_proyect.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import es.unex.gps_proyect.R;

public class InicioActivity extends AppCompatActivity{

    private BottomNavigationView bottomNavigationView;
    private FrameLayout frameLayout;


    private Fragment_creadas creadas;
    private Fragment_favoritas favoritas;
    private Fragment_Home home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        creadas = new Fragment_creadas();
        favoritas = new Fragment_favoritas();
        home = new Fragment_Home();
        bottomNavigationView = findViewById(R.id.bottomnavigationView);
        frameLayout = findViewById(R.id.frameLayout);

        //Fragment inicio
        setFragment(home);

         bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
             @Override
             public void onNavigationItemReselected(@NonNull MenuItem item) {
                 switch(item.getItemId()){
                     case R.id.FavoritasOption:
                        setFragment(favoritas);
                        break;
                     case R.id.CreadasOption:
                         setFragment(creadas);
                         break;
                     case R.id.HomeOption://Recetas
                         setFragment(home);
                         break;
                 }
             }
         });

    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout,fragment);
        fragmentTransaction.commit();

    }
}