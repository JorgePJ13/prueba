package es.unex.gps_proyect.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.data.Repository;

public class CreadasViewModel extends ViewModel {

    private final Repository mRepository;
    private LiveData<List<Receta>> mRecetas;
    private LiveData<List<Receta>> mRecetasTop;

    public CreadasViewModel(Repository repository) {
        mRepository = repository;
        mRecetas = repository.getRecetasUsu();
        mRecetasTop = repository.getRecetasFavUsu();
    }

    public LiveData<List<Receta>> getRecetas() {
        return mRecetas;
    }

    public LiveData<List<Receta>> getTopUsu() {
        return mRecetasTop;
    }
}
