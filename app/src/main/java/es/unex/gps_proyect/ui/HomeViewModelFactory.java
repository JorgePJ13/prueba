package es.unex.gps_proyect.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.gps_proyect.data.Repository;

public class HomeViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final Repository mRepository;

    public HomeViewModelFactory(Repository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new HomeViewModel(mRepository);
    }
}