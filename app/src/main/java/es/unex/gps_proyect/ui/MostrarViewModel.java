package es.unex.gps_proyect.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.data.Repository;

public class MostrarViewModel extends ViewModel {

    private final Repository mRepository;
    private LiveData<Receta> mReceta;
    private final Integer mId;

    public MostrarViewModel(Repository repository, Integer id) {
        mRepository = repository;
        mId = id;
        mReceta = repository.getRecetaId(mId);
    }

    public LiveData<Receta> getReceta() {
        return mReceta;
    }

    public void setEsFav(){
        mRepository.updateEsFav(mReceta.getValue());
    }

    public void setNoEsUsua() {
        mRepository.updateNoEsUsua(mReceta.getValue());
    }

    public void setNoEsFav(){
        mRepository.updateNoFav(mReceta.getValue());
    }

   public void setPuntuacion(Integer puntuacion){
        mRepository.updatePuntuacion(mReceta.getValue(), puntuacion);
    }

    public void deleteRec() {
        mRepository.deleteReceta(mReceta.getValue());
    }

}
