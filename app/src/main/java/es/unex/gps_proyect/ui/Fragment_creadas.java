package es.unex.gps_proyect.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.R;
import es.unex.gps_proyect.InjectorUtils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_creadas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_creadas extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView rRecetas;
    private RecetasAdapter Adapter;
    private EditText Buscador;
    private ImageView EImage;
    private TextView emptyView;
    private FloatingActionButton fab;
    private List<Receta> Recetas = new ArrayList<>();
    private CreadasViewModel mViewModel;

    public Fragment_creadas() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment_creadas.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment_creadas newInstance(String param1, String param2) {
        Fragment_creadas fragment = new Fragment_creadas();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_creadas, container, false);
        fab = vista.findViewById(R.id.floatingButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CrearRecetaActivity.class);
                startActivity(intent);
            }
        });

        EImage = (ImageView) vista.findViewById(R.id.Ayuda);
        rRecetas = (RecyclerView) vista.findViewById(R.id.recetasRecycler);
        emptyView = (TextView) vista.findViewById(R.id.empty_view2);
        rRecetas.setLayoutManager(new LinearLayoutManager(getContext()));

        //Ayuda de Usuario
        EImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
                alerta.setMessage("\nEn esta pestaña tendrá acceso a la zona de creación de recetas por parte del usuario.\nPodrá " +
                        "ver listadas cada una de las recetas que ha decidido crear, así como el acceso a la zona de creación " +
                        "mediante el botón localizado abajo a su derecha.")
                        .setCancelable(false)
                        .setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                AlertDialog titulo = alerta.create();
                titulo.setTitle("Mensaje de Ayuda");
                titulo.show();
            }
        });

        Buscador = vista.findViewById(R.id.Barra_Busc);
        Buscador.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty() == false) {
                   filtrar(editable.toString());
                }else{
                    Adapter.clear();
                    Adapter.addAll(Recetas);
                }
            }
        });

        //Inicializamos el Adapter
        List<Receta> conjuntoRecetas = new ArrayList<Receta>();
        Adapter = new RecetasAdapter(conjuntoRecetas,getActivity());
        Adapter.setItemClickListener(new RecetasAdapter.ItemClickListener() {
            public void onItemClick(Receta receta) {
                Intent intent = new Intent(getContext(), MostrarRecetaActivity.class);
                intent.putExtra("Receta", receta);
                startActivity(intent);
            }

        });

        rRecetas.setAdapter(Adapter);
        CreadasViewModelFactory factory = InjectorUtils.provideCreViewModelFactory(getActivity().getApplicationContext());
        mViewModel = ViewModelProviders.of(getActivity(), factory).get(CreadasViewModel.class);
        mViewModel.getRecetas().observe(getViewLifecycleOwner(), recetas -> {
            Adapter.swap(recetas);
            //Comprobamos si no hay recetas a mostrar, indicandoselo asi al usuario
            if (recetas.isEmpty()) {
                emptyView.setText("No Hay Recetas Creadas");
                rRecetas.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            }
            else {
                rRecetas.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
                Recetas.clear();
                Recetas.addAll(recetas);
            }
        });

        return vista;
    }

    //Filtrado de recetas de usuario
    public void filtrar (String Texto){
        List<Receta> filtrarLista = new ArrayList<>();
        for(Receta r : Adapter.getRecetas()){
            if(r.getTitle().toLowerCase().contains(Texto.toLowerCase())){
                filtrarLista.add(r);
            }
        }
        Adapter.filterList(filtrarLista);
    }

    }