package es.unex.gps_proyect;

import android.content.Context;

import es.unex.gps_proyect.AppExecutors;
import es.unex.gps_proyect.data.Network.RecetaAPI;
import es.unex.gps_proyect.data.Repository;
import es.unex.gps_proyect.ui.MostrarViewModelFactory;
import es.unex.gps_proyect.ui.EditarViewModelFactory;
import es.unex.gps_proyect.ui.CrearViewModelFactory;
import es.unex.gps_proyect.data.Roomdb.RecetaDatabase;
import es.unex.gps_proyect.ui.CreadasViewModelFactory;
import es.unex.gps_proyect.ui.FavoritasViewModelFactory;
import es.unex.gps_proyect.ui.HomeViewModelFactory;

/**
 * Provides static methods to inject the various classes needed for the app
 * Esta clase ha sido modificada para esta app de la ultima sesion de laboratorio, ViewModels
 */
public class InjectorUtils {

    public static Repository provideRepository(Context context) {
        RecetaDatabase database = RecetaDatabase.getDatabase(context.getApplicationContext());
        AppExecutors executors = AppExecutors.getInstance();
        RecetaAPI recetaAPI = RecetaAPI.getInstance(context.getApplicationContext(), executors);

        return Repository.getInstance(database.recetaDAO(), executors, recetaAPI);
    }

    public static MostrarViewModelFactory provideMostrarViewModelFactory(Context context, Integer id) {
        Repository repository = provideRepository(context.getApplicationContext());
        return new MostrarViewModelFactory(repository, id);
    }

    public static CreadasViewModelFactory provideCreViewModelFactory(Context context) {
        Repository repository = provideRepository(context.getApplicationContext());
        return new CreadasViewModelFactory(repository);
    }

    public static FavoritasViewModelFactory provideFavsViewModelFactory(Context context) {
        Repository repository = provideRepository(context.getApplicationContext());
        return new FavoritasViewModelFactory(repository);
    }

    public static HomeViewModelFactory provideHomeViewModelFactory(Context context) {
        Repository repository = provideRepository(context.getApplicationContext());
        return new HomeViewModelFactory(repository);
    }

    public static CrearViewModelFactory provideCrearViewModelFactory(Context context) {
        Repository repository = provideRepository(context.getApplicationContext());
        return new CrearViewModelFactory(repository);
    }

    public static EditarViewModelFactory provideEditarViewModelFactory(Context context) {
        Repository repository = provideRepository(context.getApplicationContext());
        return new EditarViewModelFactory(repository);
    }
}