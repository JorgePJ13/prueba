package es.unex.gps_proyect.data.Roomdb;
import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import es.unex.gps_proyect.data.Model.Receta;

@Database(entities = {Receta.class}, version = 1,exportSchema = false)
public abstract class RecetaDatabase extends RoomDatabase {

    private static RecetaDatabase instance;

    public static RecetaDatabase getDatabase(Context context){
        if(instance == null)
            instance = Room.databaseBuilder(context.getApplicationContext(), RecetaDatabase.class, "receta.db").build();
        return instance;
    }
    public abstract RecetaDAO recetaDAO();
}

