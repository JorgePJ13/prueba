package es.unex.gps_proyect.data;

import androidx.lifecycle.LiveData;

import java.util.List;

import es.unex.gps_proyect.AppExecutors;
import es.unex.gps_proyect.data.Model.Receta;
import es.unex.gps_proyect.data.Roomdb.RecetaDAO;
import es.unex.gps_proyect.data.Network.RecetaAPI;

public class Repository {

    private static Repository sRepository;
    private static final Object OBJECT = new Object();
    private final RecetaDAO mRecetaDao;
    private final AppExecutors mExecutors;
    private final RecetaAPI mRecetaAPI;

    public Repository(RecetaDAO recetaDAO, AppExecutors executors, RecetaAPI recetaAPI){
        mRecetaDao = recetaDAO;
        mExecutors = executors;
        mRecetaAPI = recetaAPI;
    }

    //Para instanciar la base de datos, patron Singleton
    public synchronized static Repository getInstance(
            RecetaDAO recetaDAO, AppExecutors executors, RecetaAPI recetaAPI) {
        //Obteniendo rectas del repositorio
        if (sRepository == null) {
            synchronized (OBJECT) {
                sRepository = new Repository(recetaDAO, executors, recetaAPI);
            }
        }
        return sRepository;
    }

    //Metodos de llamada a la base de datos

    //Total de las recetas almacenadas en la base de datos
    public LiveData<List<Receta>> getRecetas() {
        return mRecetaDao.getAll();
    }

    //Conjunto de recetas favoritas marcadas por el usuario
    public LiveData<List<Receta>> getRecetasFav() {
        return mRecetaDao.getFavs();
    }

    //Conjunto de recetas top marcadas por el usuario
    public LiveData<List<Receta>> getTop() {
        return mRecetaDao.getRecePuntu();
    }

    //Conjunto de las recetas creadas por el usuario
    public LiveData<List<Receta>> getRecetasUsu() {
        return mRecetaDao.getReceCreadas();
    }

    //Conjunto de las recetas favs top del usurio
    public LiveData<List<Receta>> getRecetasFavTop() {
        return mRecetaDao.getReceFavTop();
    }

    //Conjunto de las recetas creadas top del usurio
    public LiveData<List<Receta>> getRecetasFavUsu() {
        return mRecetaDao.getReceFavUsu();
    }

    //Obtenemos una receta de la base de datos
    public LiveData<Receta> getRecetaId(Integer id){
        return mRecetaDao.getReceta(id);
    }

    //Procedimiento para buscar receta en la BD
    Receta buscarReceta(Receta R, List<Receta> RBD){
        boolean enc=false;
        int i=0; //indice
        Receta rBd = null;
        while(i<RBD.size() && enc==false){
            rBd = RBD.get(i);
            if(R.getTitle().equals(rBd.getTitle())==true){
                enc = true;
            }
            i++;
        }
        //Para eviatar que se haga una ultima insercion
        if(enc == false){
            rBd = null;
        }
        return rBd;
    }

    //Anadimos una receta a la base de datos
    public void addReceta(Receta receta) {
        mExecutors.diskIO().execute(() -> {
            mRecetaDao.insert(receta);
        });
    }

    //Anadimos una receta a la base de datos
    public void updateReceta(Receta receta) {
        mExecutors.diskIO().execute(() -> {
            mRecetaDao.updateReceta(receta);
        });
    }

    //Anadimos receta a favoritas
    public void updateEsFav(Receta receta) {
        mExecutors.diskIO().execute(() -> {
            mRecetaDao.setFavorita(receta.getId());
        });
    }

    //Borramos receta de favoritas
    public void updateNoFav(Receta receta) {
        mExecutors.diskIO().execute(() -> {
            mRecetaDao.quitarFavorita(receta.getId());
        });
    }

    //Anadimos puntuacion a la receta
    public void updatePuntuacion(Receta receta, Integer puntuacion) {
        mExecutors.diskIO().execute(() -> {
            mRecetaDao.setPuntuacion(receta.getId(), puntuacion);
        });
    }

    public void updateNoEsUsua(Receta receta) {
        mExecutors.diskIO().execute(() -> {
            mRecetaDao.setNoEsUsua(receta.getId());
        });
    }


    //Eliminamos una receta de la base de datos
    public void deleteReceta(Receta receta) {
        mExecutors.diskIO().execute(() -> {
            mRecetaDao.deleteReceta(receta);
        });
    }

    //Evitamos repetidas, a la hora de insercion de recetas puntuadas
    public void quitarRepetidas(List<Receta> RAPI) {
        Receta rBd;//Aux
        List<Receta> RBD;
        RBD = mRecetaDao.getAll().getValue();

        if (RBD != null) {
            for (Receta rApi : RAPI) {
                //Buscamos la receta de la API en la base de datos, por si actualizada
                rBd = buscarReceta(rApi, RBD);
                if (rBd != null) {//Si no existe en la BD, inserto la de la API
                    //Actualizacion
                    rApi.setEsFav(rBd.isEsFav());
                    rApi.setPuntuacion(rBd.getPuntuacion());
                }
                mRecetaDao.updateReceta(rApi);
                rBd = null;
            }
        } else {
            //La primera carga si la bd esta vacia
            for (Receta rApi : RAPI) {
                mRecetaDao.insert(rApi);
            }
        }
    }

    //Obtencion de los datos tras lectura en la API
    public void getAll(){
        LiveData<List<Receta>> recetasAPI = mRecetaAPI.getAll();
        recetasAPI.observeForever(recetasAleer -> {
            mExecutors.diskIO().execute(() -> {
                if((recetasAleer.size() > 0)){
                    //Devuelve la lista que tiene todas las recetas
                    quitarRepetidas(recetasAleer);
                }
            });
        });
    }

}

