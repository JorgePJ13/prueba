package es.unex.gps_proyect.data.Model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "recetas")
public class Receta implements Serializable
{

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;
    @ColumnInfo(name = "titulo")
    @SerializedName("title")
    @Expose
    private String title;
    @ColumnInfo(name = "resumen")
    @SerializedName("summary")
    @Expose
    private String summary;
    @ColumnInfo(name = "imagen")
    @SerializedName("image")
    @Expose
    private String image;
    @ColumnInfo(name = "web")
    @SerializedName("spoonacularSourceUrl")
    @Expose
    private String spoonacularSourceUrl;
    @ColumnInfo(name = "comensales")
    @SerializedName("servings")
    @Expose
    private Integer servings;
    @ColumnInfo(name = "preparacion")
    @SerializedName("readyInMinutes")
    @Expose
    private Integer readyInMinutes;
    @ColumnInfo(name = "vegetariana")
    @SerializedName("vegetarian")
    @Expose
    private Boolean vegetarian;
    @ColumnInfo(name = "vegana")
    @SerializedName("vegan")
    @Expose
    private Boolean vegan;
    @ColumnInfo(name = "sinGluten")
    @SerializedName("glutenFree")
    @Expose
    private Boolean glutenFree;
    @ColumnInfo(name = "sinLacteos")
    @SerializedName("dairyFree")
    @Expose
    private Boolean dairyFree;
    @ColumnInfo(name = "saludable")
    @SerializedName("veryHealthy")
    @Expose
    private Boolean veryHealthy;
    @ColumnInfo(name = "likesWeb")
    @SerializedName("aggregateLikes")
    @Expose
    private Integer aggregateLikes;
    @ColumnInfo(name = "puntuacionWeb")
    @SerializedName("spoonacularScore")
    @Expose
    private Double spoonacularScore;
    @ColumnInfo(name = "descripcion")
    private String descripcion;
    @ColumnInfo(name = "puntuacion")
    private Integer puntuacion;
    @ColumnInfo(name = "recetaUsuario")
    private boolean esUsua; //Indica si la receta ha sido creada por un usuario
    @ColumnInfo(name = "favorita")
    private boolean esFav; //Indica si la receta es escogida como favorita

    private final static long serialVersionUID = -8730140962906357938L;


    public Receta(String title, String image, Integer servings, Integer readyInMinutes, Boolean vegetarian, Boolean vegan, Boolean glutenFree, Boolean dairyFree, Boolean veryHealthy, String descripcion, Integer puntuacion) {
        this.title = title;
        this.image = image;
        this.servings = servings;
        this.readyInMinutes = readyInMinutes;
        this.vegetarian = vegetarian;
        this.vegan = vegan;
        this.glutenFree = glutenFree;
        this.dairyFree = dairyFree;
        this.veryHealthy = veryHealthy;
        this.descripcion = descripcion;
        this.puntuacion = puntuacion;
        this.esUsua = true;
        this.esFav = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Integer getPuntuacion() {
        return puntuacion;
    }

    public boolean isEsUsua() {
        return esUsua;
    }

    public boolean isEsFav() {
        return esFav;
    }

    public void setPuntuacion(Integer puntuacion) {
        this.puntuacion = puntuacion;
    }

    public void setEsUsua(boolean esUsua) {
        this.esUsua = esUsua;
    }

    public void setEsFav(boolean esFav) {
        this.esFav = esFav;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSpoonacularSourceUrl() {
        return spoonacularSourceUrl;
    }

    public void setSpoonacularSourceUrl(String spoonacularSourceUrl) {
        this.spoonacularSourceUrl = spoonacularSourceUrl;
    }

    public Integer getServings() {
        return servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }

    public Integer getReadyInMinutes() {
        return readyInMinutes;
    }

    public void setReadyInMinutes(Integer readyInMinutes) {
        this.readyInMinutes = readyInMinutes;
    }

    public Boolean getVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(Boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public Boolean getVegan() {
        return vegan;
    }

    public void setVegan(Boolean vegan) {
        this.vegan = vegan;
    }

    public Boolean getGlutenFree() {
        return glutenFree;
    }

    public void setGlutenFree(Boolean glutenFree) {
        this.glutenFree = glutenFree;
    }

    public Boolean getDairyFree() {
        return dairyFree;
    }

    public void setDairyFree(Boolean dairyFree) {
        this.dairyFree = dairyFree;
    }

    public Boolean getVeryHealthy() {
        return veryHealthy;
    }

    public void setVeryHealthy(Boolean veryHealthy) {
        this.veryHealthy = veryHealthy;
    }

    public Integer getAggregateLikes() {
        return aggregateLikes;
    }

    public void setAggregateLikes(Integer aggregateLikes) {
        this.aggregateLikes = aggregateLikes;
    }

    public Double getSpoonacularScore() {
        return spoonacularScore;
    }

    public void setSpoonacularScore(Double spoonacularScore) {
        this.spoonacularScore = spoonacularScore;
    }

    @Override
    public String toString() {
        return "Receta{" +
                "title='" + title + '\'' +
                ", summary='" + summary + '\'' +
                ", image='" + image + '\'' +
                ", spoonacularSourceUrl='" + spoonacularSourceUrl + '\'' +
                ", servings=" + servings +
                ", readyInMinutes=" + readyInMinutes +
                ", vegetarian=" + vegetarian +
                ", vegan=" + vegan +
                ", glutenFree=" + glutenFree +
                ", dairyFree=" + dairyFree +
                ", veryHealthy=" + veryHealthy +
                ", aggregateLikes=" + aggregateLikes +
                ", spoonacularScore=" + spoonacularScore +
                ", descripcion='" + descripcion + '\'' +
                ", puntuacion=" + puntuacion +
                ", esUsua=" + esUsua +
                ", esFav=" + esFav +
                '}';
    }

}
