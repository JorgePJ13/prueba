package es.unex.gps_proyect.data.Network;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import es.unex.gps_proyect.AppExecutors;
import es.unex.gps_proyect.data.Model.ListaRecetas;
import es.unex.gps_proyect.data.Model.Receta;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public class RecetaAPI {

    private static final String LOG_TAG = RecetaAPI.class.getSimpleName();
    private static RecetaAPI sInstance;

    private final MutableLiveData<List<Receta>> mRecetas;
    //Lista Auxiliar
    private List<Receta> recetasFinales = new ArrayList<>();
    private final Context mContext;
    private final AppExecutors mExecutors;
    private static final Object OBJECT = new Object();

    //Interfaz para conectarnos por RetroFit a la API externa
    public interface SpoonacularDBService{
        //Obtencion del conjunto de recetas
        @GET("{buscar}/complexSearch?apiKey=760a2e2f1161451ca1424003cf653219")
        Call<ListaRecetas> listRecetas(@Path("buscar") String buscar);

        //Obtencion de la informacion de la receta
        @GET("recipes/{id}/information?includeNutrition=false&apiKey=760a2e2f1161451ca1424003cf653219")
        Call<Receta> infoReceta(@Path("id") Integer id);
    }

    //Constructor parametrizado de la instancia
    public RecetaAPI(Context context, AppExecutors executors){
        mContext = context;
        mExecutors = executors;
        mRecetas = new MutableLiveData<>();
    }

    //Singleton
    public static RecetaAPI getInstance(Context context, AppExecutors executors) {
        if (sInstance == null) {
            synchronized (OBJECT) {
                sInstance = new RecetaAPI(context.getApplicationContext(), executors);
            }
        }
        return sInstance;
    }

    //Objeto LiveData
    public LiveData<List<Receta>> getAll() {
        GetAPIData();
        return mRecetas;
    }

    private void GetAPIData() {

        try {
            Log.i("datos recetas", "creado el servicio ");
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.spoonacular.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

           /* Log.i("datos recetas", "creado el servicio ");

            SpoonacularDBService service = retrofit.create(SpoonacularDBService.class);

            Log.i("datos recetas", "creado el servicio ");

            Call<ListaRecetas> call = service.listRecetas("recipes");

            Log.i("recetas", "creado el servicio ");

            call.enqueue(new Callback<ListaRecetas>() {
                @Override
                public void onResponse(Call<ListaRecetas> call, Response<ListaRecetas> response) {
                    Log.i("datos recetas", response.toString());
                    if (response.isSuccessful()) {
                        //Recetas leidas de la API
                        ListaRecetas recetas = response.body();
                        mRecetas.postValue(recetas.getRecetas());
                    } else {
                        //Mensaje de error
                        Log.d("Receta", "RESPONSE NOOK");
                    }
                }

                @Override
                public void onFailure(Call<ListaRecetas> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                }
            });*/

            Log.i("datos recetas", "creado el servicio ");

            SpoonacularDBService service = retrofit.create(SpoonacularDBService.class);

            Log.i("datos recetas", "creado el servicio ");

            Call<ListaRecetas> call = service.listRecetas("recipes");

            Log.i("recetas", "creado el servicio ");

            call.enqueue(new Callback<ListaRecetas>() {
                @Override
                public void onResponse(Call<ListaRecetas> call, Response<ListaRecetas> response) {
                    Log.i("datos recetas", response.toString());
                    if (response.isSuccessful()) {
                        //Recetas leidas de la API
                        ListaRecetas recetas = response.body();
                        //Informacion para las recetas
                        for(Receta r : recetas.getRecetas()){
                            Log.i("datos de recetas", "creado el servicio ");

                            Call<Receta> call1 = service.infoReceta(r.getId());

                            Log.i("datos recetas", "creado el servicio ");

                            call1.enqueue(new Callback<Receta>() {
                                @Override
                                public void onResponse(Call<Receta> call1, Response<Receta> response) {
                                    Log.i("datos receta", response.toString());
                                    if (response.isSuccessful()) {
                                        //Receta con toda la informacion
                                        Receta receta = response.body();
                                        recetasFinales.add(receta);
                                    } else {
                                        //Mensaje de error
                                        Log.d("Receta", "RESPONSE NOOK");
                                    }
                                }

                                @Override
                                public void onFailure(Call<Receta> call1, Throwable t) {
                                    Log.d("Error", t.getMessage());
                                }
                            });
                        }
                        mRecetas.postValue(recetasFinales);
                    } else {
                        //Mensaje de error
                        Log.d("Receta", "RESPONSE NOOK");
                    }
                }

                @Override
                public void onFailure(Call<ListaRecetas> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                }
            });
            //

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
