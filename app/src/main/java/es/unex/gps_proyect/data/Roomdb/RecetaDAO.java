package es.unex.gps_proyect.data.Roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.gps_proyect.data.Model.Receta;

@Dao
public interface RecetaDAO {

    @Query("SELECT * FROM recetas ORDER BY titulo ASC")
    public LiveData<List<Receta>> getAll();

    @Query("SELECT * FROM recetas WHERE favorita = 1 ORDER BY titulo ASC")
    public LiveData<List<Receta>> getFavs();

    @Query("SELECT * FROM recetas WHERE Id = :uid")
    public LiveData<Receta> getReceta(Integer uid);

    @Query("SELECT * FROM recetas WHERE titulo = :titulo")
    public Receta getRecetaByTitle(String titulo);

    @Query("SELECT * FROM recetas WHERE puntuacion >= 5 ORDER BY puntuacion DESC")
    public LiveData<List<Receta>> getRecePuntu();

    @Query("SELECT * FROM recetas WHERE recetaUsuario = 1 ORDER BY titulo ASC")
    public LiveData<List<Receta>> getReceCreadas();

    @Query("SELECT * FROM recetas WHERE favorita = 1 AND puntuacion >= 5 ORDER BY puntuacion DESC")
    public LiveData<List<Receta>> getReceFavTop();

    @Query("SELECT * FROM recetas WHERE favorita = 1 AND recetaUsuario = 1 AND puntuacion >= 5 ORDER BY titulo ASC")
    public LiveData<List<Receta>> getReceFavUsu();

    @Query("UPDATE recetas SET favorita = 1 WHERE id = :uid")
    public void setFavorita(Integer uid);

    @Query("UPDATE recetas SET recetaUsuario = 0 WHERE id = :uid")
    public void setNoEsUsua(Integer uid);

    @Query("UPDATE recetas SET favorita = 0 WHERE id = :uid")
    public void quitarFavorita(Integer uid);

    @Query("UPDATE recetas SET puntuacion = :puntos WHERE id = :uid")
    public void setPuntuacion(Integer uid, Integer puntos);

    @Query("SELECT * FROM recetas WHERE Id = :uid")
    public Receta getRecetaNoLive(Integer uid);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public void insert(Receta item);

    @Delete
    public void deleteReceta(Receta item);

    @Update
    public void updateReceta(Receta item);

}
